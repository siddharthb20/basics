# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 12:03:56 2022

@author: Siddharth
"""

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import r2_score
from sklearn import preprocessing
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
import os
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.compose import TransformedTargetRegressor
from sklearn.ensemble import GradientBoostingRegressor
#%%

warnings.filterwarnings("ignore")

current_dir = os.getcwd()
files = os.listdir(current_dir)
for f in files:
    if '.csv' in f:
        file_loc = os.path.join(current_dir, f)
        break
df = pd.read_csv(file_loc)

# Segregating input and output
x = df.iloc[:, :12]
y = df.iloc[:, 12]

print(x.columns)

# x = x.values
# normalizer = preprocessing.Normalizer()
# x_scaled = normalizer.fit_transform(x)
# x = pd.DataFrame(x_scaled)

sns.pairplot(data=x, diag_kind='kde')
#%%
fig, ax2 = plt.subplots(3, 4, constrained_layout=True)
sns.countplot(ax=ax2[0, 0], x=df.day)
sns.countplot(ax=ax2[0, 1], x=df.mnth)
sns.countplot(ax=ax2[0, 2], x=df.year)
sns.countplot(ax=ax2[0, 3], x=x.season)
sns.countplot(ax=ax2[1, 0], x=x.holiday)
sns.countplot(ax=ax2[1, 1], x=x.weekday)
sns.countplot(ax=ax2[1, 2], x=x.workingday)
sns.countplot(ax=ax2[1, 3], x=x.weathersit)
sns.countplot(ax=ax2[2, 0], x=x.temp)
sns.countplot(ax=ax2[2, 1], x=x.atemp)
sns.countplot(ax=ax2[2, 2], x=x.hum)
sns.countplot(ax=ax2[2, 3], x=x.windspeed)
plt.show()
#%%
fig, ax2 = plt.subplots(3, 3, constrained_layout=True)
sns.boxplot(ax=ax2[0, 0], x=df.day, y=y)
sns.boxplot(ax=ax2[0, 1], x=df.mnth, y=y)
sns.boxplot(ax=ax2[0, 2], x=df.year, y=y)
sns.boxplot(ax=ax2[1, 1], x=x.season, y=y)
sns.boxplot(ax=ax2[1, 0], x=x.holiday, y=y)
sns.boxplot(ax=ax2[1, 2], x=x.weekday, y=y)
sns.boxplot(ax=ax2[2, 1], x=x.workingday, y=y)
sns.boxplot(ax=ax2[2, 2], x=x.weathersit, y=y)
plt.show()
#%%
x_train, x_test, y_train, y_test = train_test_split(x, y, \
                                                    test_size=0.2, \
                                                    )
#%%
feat2scale = ['day', 'mnth', 'year', 'season', 'weekday']
sc = StandardScaler()
x_train[feat2scale] = sc.fit_transform(x_train[feat2scale])
x_test[feat2scale] = sc.transform(x_test[feat2scale])
#%%
# Training the model - Linear Regression
lin_reg = LinearRegression()
lin_reg.fit(x_train, y_train)
y_pred = lin_reg.predict(x_test)

accuracy = r2_score(y_test, y_pred)*100
print('Model accuracy: %.2f' % accuracy)

# Training the model - Decision Trees
dec_tre = DecisionTreeRegressor()
dec_tre.fit(x_train, y_train)
y_pred = dec_tre.predict(x_test)

accuracy = r2_score(y_test, y_pred)*100
print('Model accuracy: %.2f' % accuracy)

# Training the model - Gradient Boosting regressor
gbr = GradientBoostingRegressor()
gbr.fit(x_train, y_train)
y_pred = gbr.predict(x_test)

accuracy = r2_score(y_test, y_pred)*100
print('Model accuracy: %.2f' % accuracy)
#%%
param_grid = {'n_estimators': [100, 80, 60, 50, 40],
              'max_depth': [3, 4, 5, 6],
              'learning_rate': [0.2, 0.05, 0.1, 0.01]}

grid = GridSearchCV(GradientBoostingRegressor(), param_grid, refit=True, verbose=0)

reg_tran = TransformedTargetRegressor(regressor=grid)
grid_result = reg_tran.fit(x_train, y_train)
y_pred = reg_tran.predict(x_test)
accuracy = r2_score(y_test, y_pred)*100
best_param = grid_result.regressor_.best_params_
print('Best hyperparameters:', best_param)
print('Model accuracy: %.2f' % accuracy)
