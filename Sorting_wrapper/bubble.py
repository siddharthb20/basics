# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 12:23:35 2022

@author: Siddharth
"""
import copy 

def bubbleSort(arr):
    
    print(arr)
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)):
        for j in range(len(sorted_arr)-i-1):
            if sorted_arr[j] > sorted_arr[j+1]:
                temp = sorted_arr[j]
                sorted_arr[j] = sorted_arr[j+1]
                sorted_arr[j+1] = temp
        print(sorted_arr)
    return sorted_arr