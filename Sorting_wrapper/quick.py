# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 12:25:07 2022

@author: Siddharth
"""

import copy

def quickSort(arr):
    
    sorted_arr = copy.deepcopy(arr)
    if len(sorted_arr) > 0:
        pivot = sorted_arr[-1]
        left = []
        right = []
        for i in range(len(sorted_arr)-1):
            if sorted_arr[i] < pivot:
                left.append(sorted_arr[i])
            else:
                right.append(sorted_arr[i])   
        print(f'Left: {left}, Right:{right}, Pivot:{pivot}')
        left = quickSort(left)
        right = quickSort(right)
        if left is None:
            left = []
        if right is None:
            right = []
        left.append(pivot)
        left.extend(right)
        if len(left)>0:
            print(left)
        
        return left