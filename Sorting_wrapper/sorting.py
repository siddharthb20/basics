# -*- coding: utf-8 -*-
"""
Created on Sun Jul 17 10:52:14 2022

@author: Siddharth
"""

import timeit
import copy
# from bubble import bubbleSort
# from selection import selectionSort
from merge import mergeSort
# from insertion import insertionSort
from quick import quickSort       


def time_it(func):
    def wrapper(*args, **kwargs):
        start = timeit.default_timer()
        sorted_arr = func(*args, **kwargs)
        end = timeit.default_timer()
        print(func.__name__, " time:", (end-start))
        return sorted_arr
    return wrapper


@time_it
def bubbleSort(arr):
    
    print(arr)
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)):
        for j in range(len(sorted_arr)-i-1):
            if sorted_arr[j] > sorted_arr[j+1]:
                temp = sorted_arr[j]
                sorted_arr[j] = sorted_arr[j+1]
                sorted_arr[j+1] = temp
        print(sorted_arr)
    return sorted_arr


@time_it
def selectionSort(arr):
    
    print(arr)
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)):
        min_value = i
        for j in range(i, len(sorted_arr)):
    
            if sorted_arr[j]<arr[min_value]:
                min_value = j
        temp = sorted_arr[i]
        sorted_arr[i] = sorted_arr[min_value]
        sorted_arr[min_value] = temp
        print(sorted_arr)
    return sorted_arr

# @time_it
# def mergeSort(arr):
    
#     sorted_arr = copy.deepcopy(arr)
#     if len(sorted_arr) > 1:
#         mid = len(sorted_arr)//2
#         left = sorted_arr[:mid]
#         right = sorted_arr[mid:]
#         mergeSort(left)
#         mergeSort(right)
        
#         i, j, k = 0, 0, 0
#         while i < len(left) and j < len(right):
#             if left[i] < right[j]:
#                 sorted_arr[k] = left[i]
#                 i += 1
#                 k += 1
#             else:
#                 sorted_arr[k] = right[j]
#                 j += 1
#                 k += 1
        
#         while i < len(left):
#             sorted_arr[k] = left[i]
#             i += 1
#             k += 1
  
#         while j < len(right):
#             sorted_arr[k] = right[j]
#             j += 1
#             k += 1
        
#         print(f'Left: {left}, Right: {right}, Merged: {arr}')
#         return sorted_arr
    
# @time_it
# def quickSort(arr):
    
#     sorted_arr = copy.deepcopy(arr)
#     if len(sorted_arr) > 0:
#         pivot = sorted_arr[-1]
#         left = []
#         right = []
#         for i in range(len(sorted_arr)-1):
#             if sorted_arr[i] < pivot:
#                 left.append(sorted_arr[i])
#             else:
#                 right.append(sorted_arr[i])   
#         print(f'Left: {left}, Right:{right}, Pivot:{pivot}')
#         left = quickSort(left)
#         right = quickSort(right)
#         if left is None:
#             left = []
#         if right is None:
#             right = []
#         left.append(pivot)
#         left.extend(right)
#         if len(left)>0:
#             print(left)
        
#         return left


@time_it
def insertionSort(arr):

    print(arr)    
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)-1):
        current_value = i+1
        j = i
        temp = sorted_arr[current_value]
        while j>=0 and temp<sorted_arr[j]:
            sorted_arr[j+1] = sorted_arr[j]
            j -= 1
        sorted_arr[j+1] = temp
        print(sorted_arr)
                
    return sorted_arr

arr = [93, 65, 23, 96, 34, 76, 12, 73, 9, 87, 15]
print("--------------Bubble Sort------------------")
bubble = bubbleSort(arr)
print("\n-------------Selection Sort----------------")
select = selectionSort(arr)
print("\n--------------Merge Sort-------------------")
print(arr)
start = timeit.default_timer()
merge = mergeSort(arr)
print("Sorting time: ", timeit.default_timer()-start)
print("\n-------------Insertion Sort----------------")
insert = insertionSort(arr)
print("\n--------------Quick Sort-------------------")
print(arr)
start = timeit.default_timer()
quick = quickSort(arr)
print("Sorting time: ", timeit.default_timer()-start)
#print(bubble_sort_arr)
