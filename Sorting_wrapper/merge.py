# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 12:24:48 2022

@author: Siddharth
"""
import copy 

def mergeSort(arr):
    
    sorted_arr = copy.deepcopy(arr)
    if len(sorted_arr) > 1:
        mid = len(sorted_arr)//2
        left = sorted_arr[:mid]
        right = sorted_arr[mid:]
        mergeSort(left)
        mergeSort(right)
        
        i, j, k = 0, 0, 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                sorted_arr[k] = left[i]
                i += 1
                k += 1
            else:
                sorted_arr[k] = right[j]
                j += 1
                k += 1
        
        while i < len(left):
            sorted_arr[k] = left[i]
            i += 1
            k += 1
  
        while j < len(right):
            sorted_arr[k] = right[j]
            j += 1
            k += 1
        
        print(f'Left: {left}, Right: {right}, Merged: {arr}')
        return sorted_arr