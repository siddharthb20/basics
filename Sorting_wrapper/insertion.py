# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 12:24:24 2022

@author: Siddharth
"""
import copy

def insertionSort(arr):

    print(arr)    
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)-1):
        current_value = i+1
        j = i
        temp = sorted_arr[current_value]
        while j>=0 and temp<sorted_arr[j]:
           sorted_arr[j+1] = sorted_arr[j]
           j -= 1
        sorted_arr[j+1] = temp
        print(sorted_arr)
                
    return sorted_arr