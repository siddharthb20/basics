# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 12:23:59 2022

@author: Siddharth
"""

import copy

def selectionSort(arr):
    
    print(arr)
    sorted_arr = copy.deepcopy(arr)
    for i in range(len(sorted_arr)):
        min_value = i
        for j in range(i, len(sorted_arr)):
    
            if sorted_arr[j]<arr[min_value]:
                min_value = j
        temp = sorted_arr[i]
        sorted_arr[i] = sorted_arr[min_value]
        sorted_arr[min_value] = temp
        print(sorted_arr)
    return sorted_arr