# Implementing various sorting algorithms and measuring their time

## Algorithms implemented

Bubble sort <br />
Selection sort <br />
Insertion sort <br />
Merge sort <br />
Quick sort <br />

## Wrapper

Implemented a decorator to measure time for the above mentioned sorting algorithms. However, using decorator for merge and quick sort is not a good idea because these methods use recursion and the wrapper function is called for intermediate steps also. This leads to unnecessary printing of time for intermediate steps also. 

