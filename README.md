# Basics

Simple python scripts to practise python features and basic ML projects

## Regression

Simple regression implementation using scikit learn and using bike usage dataset from Azure

## Wrapper

Practising wrappers in Python using sorting algorithms as examples

